<?php

    require_once("./database/Database.php");

    class DatabaseTest extends \PHPUnit\Framework\TestCase {
        
            // testing the database connection function
        public function testConnectionToDb(){

            $mockApp = $this->createMock(Database::class);
            $connect = TRUE;
        
            $mockApp->method('con')->willReturn($connect);
            
            $conn = $mockApp->con("localhost","root","","mik");
        
            
            $this->assertSame($conn,TRUE);
        
            //var_dump($conn);
        }
            // testing the database creation function
        public function testCreateDb(){
            $mockApp = $this->createMock(Database::class);
        
            $result = "Created";
        
            $mockApp->method('create_db')->willReturn($result);
            
            $creat = $mockApp->create_db("localhost","root","","mik");
            
    
            $this->assertEquals($creat,"Created");
            //var_dump($conn);
        }
    }