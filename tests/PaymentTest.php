<?php

    //importing the user module module
    require_once("./modules/Meter.php");
    require_once("./modules/Billing.php");
    require_once("./modules/Payments.php");


    class PaymentTest extends \PHPUnit\Framework\TestCase {

            // testing the add payment function
        public function testAddPayment(){

            $meter_id = "1234567";
            $customer_id = "098765423";
             $amount = 20;
             $transaction_id = 123456; 
             $user_email = "yy@ym.com";
            $myMock = $this->createMock(Payment::class);
            $myMock->method('add_payment')->willReturn("Success");
            $result = $myMock->add_payment($meter_id,$customer_id, $amount, $transaction_id, $user_email);
            $this->assertTrue($result == "Success");
        }

            // testing the add direct payment function
        public function testAddDirectPayment(){

            $meter_id = "1234567";
            $customer_id = "098765423";
             $amount = 20;
             $transaction_id = 123456; 
             $user_email = "yy@ym.com";
             $amount_paid = 20;
             $paid_status ='Paid';
             $phone_no = 0543524033;
             $payment_method = "MoMo";

            $myMock = $this->createMock(Payment::class);
            $myMock->method('add_payment_direct')->willReturn("Success");
            $result = $myMock->add_payment_direct($meter_id,$customer_id, $amount, $amount_paid,
            $paid_status, $transaction_id, $user_email, $phone_no, $payment_method);
            $this->assertTrue($result == "Success");
        }

            // testing the check status function
        public function testCheckStatus(){

            $meter_id = "1234567";
            
             $transaction_id = 123456; 
             
            $myMock = $this->createMock(Payment::class);
            $myMock->method('check_status')->willReturn("Paid" || "Pending");
            $result = $myMock->check_status($meter_id,$transaction_id);
            $this->assertEquals($result, TRUE);
        }

            // testing the update payment function
        public function testUpdatePayment(){

            $meter_id = "1234567";
        
             $transaction_id = 123456; 
             $amount_paid = 20;
             $paid_status ='Paid';
             $phone_no = 0543524033;
             $payment_method = "MoMo"; 
             
            $myMock = $this->createMock(Payment::class);
            $myMock->method('update_payment')->willReturn("Success");
            $result = $myMock->update_payment($meter_id,$amount_paid,
            $paid_status,$transaction_id, $phone_no, $payment_method);
            $this->assertEquals($result, "Success");
        }

            // testing the get payment function
        public function testGetPayment(){
            $meter_id = "12345678";
            $customer_id = 1234567;
            $no = 0;
            $myMock = $this->createMock(Payment::class);
            $myMock->method('get_payments')->willReturn([]);
            $result = $myMock->get_payments($meter_id,$customer_id,$no);
            $this->assertIsArray($result);
        }

            // testing the get payment sum function
        public function testGetPaymentSum(){
            $meter_id = "12345678";
            $customer_id = 1234567;
            $myMock = $this->createMock(Payment::class);
            $myMock->method('get_payment_sum')->willReturn(10.00);
            $result = $myMock->get_payment_sum($meter_id,$customer_id);
            $this->assertIsNumeric($result);
        }
    }