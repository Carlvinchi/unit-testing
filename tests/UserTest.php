<?php
        //importing the user module module
        require_once("./modules/User.php");

    class UserTest extends \PHPUnit\Framework\TestCase {

            // testing the signup function
        public function testSignup(){
            $myMock = $this->createMock(User::class);

            $create_result = "Success";
            $myMock->method('create_user')->willReturn($create_result);
            
            $user_email  = "9069@gmail.com";
            $phone_no = "0543524033";
            $first_name = "Confid";
            $last_name = "Antwi";
            $digital_address= "AH-123-456-789";
            $address_street = "Kokoben";
            $address_city = "Kumasi";
            $address_region = "Ashanti";
            $password = "Realerno@20";
        
            $res = $myMock->create_user($user_email,$phone_no,$first_name,$last_name,
            $digital_address,$address_street,$address_city,$address_region,$password);
            $this->assertEquals($res,"Success");
            
        }

            // testing the login function
        public function testLogin(){
            $myMock = $this->createMock(User::class);

            $user_email  = "9069@gmail.com";
            $password = "Realerno@20";
            $create_result = "Success";
            $myMock->method('user_login')->willReturn($create_result);

            $myLogin = $myMock->user_login($user_email, $password);
            $this->assertEquals($myLogin,"Success");

        }

            // testing the user exist  function
        public function testUserExist(){
            $myMock = $this->createMock(User::class);
            $user_email  = "9069@gmail.com";
            $create_result = "";
            $myMock->method('userExists')->willReturn($create_result);
            $myExist = $myMock->userExists($user_email);
            $this->assertIsString($myExist,"");
           

        }

            // testing the change password function
        public function testChangePassword(){
            $myMock = $this->createMock(User::class);
            $old_password = "1234567";
            $new_password = "Real@34";
            $user_id = 2;
            $create_result = "Success";
            $myMock->method('change_password')->willReturn($create_result);
            $myChange = $myMock->change_password($old_password,$new_password,$user_id);
            $this->assertEquals($myChange,"Success");
        }

            // testing the edit profile function
        public function testEditProfile(){

            $user_email  = "9069@gmail.com";
            $phone_no = "0543524033";
            $first_name = "Confid";
            $last_name = "Antwi";
            $digital_address= "AH-123-456-789";
            $address_street = "Kokoben";
            $address_city = "Kumasi";
            $address_region = "Ashanti";
            $user_id = 2;

            $myMock = $this->createMock(User::class);

            $create_result = "Success";
            $myMock->method('edit_profile_no_password')->willReturn($create_result);
            $myEdit = $myMock->edit_profile_no_password($user_email,$phone_no,$first_name,$last_name,
            $digital_address,$address_street,$address_city,$address_region,$user_id);

            $this->assertEquals($myEdit,"Success");
        }

            // testing the find function
        public function testFind(){
            $data = [
                
                [ "user_id" => 1 , "customer_id" => 981622203618136, "user_email" =>"me@yahoo.com" , 
                "phone_no" => 0543524033, "first_name" => "Confid", "last_name" => "Antwi",
                 "digital_address" => "AH-123-456-789", "address_street" => "Kokoben", 
                 "address_city" => "Kumasi", "address_region" => "Ashanti", 
                 "password" => "$2y$15oxJz1jSB82yKA.jaU16dt.reFooxnLfDoFruFNryVa1ZUPJYx2rTO", "reg_date" => "2021-05-28 12:06:58" ,"last_login" =>""
                ],
                [ "user_id" => 2 , "customer_id" => 98162220361834, "user_email" => "kk@yahoo.com", 
                "phone_no" => 0543524033, "first_name" => "Confid", "last_name" => "Antwi",
                 "digital_address" => "AH-123-456-789", "address_street" => "Kokoben", 
                 "address_city" => "Kumasi", "address_region" => "Ashanti", 
                 "password" => "$2y$15oxJz1jSB82yKA.jaU16dt.reFooxnLfDoFruFNryVa1ZUPJYx2rTO", "reg_date" => "2021-05-28 12:06:58" ,"last_login" =>""
                ]
            ];

            $myMock = $this->createMock(User::class);
            $myMock->method('find')->willReturn($data);
            $myFind = $myMock->find();
            $this->assertIsArray($myFind);

        }
    }