<?php

    //importing the user module module
    require_once("./modules/Meter.php");

    class MeterTest extends \PHPUnit\Framework\TestCase {

        // testing the add meter function responses
        public function testAddMeterResp(){
            $meter_id = "1234569845"; 
            $meter_account = 25.00 ;
            $meter_owner = "Cyto Owusu"; 
            $meter_address = "Ashtown, Kumasi";
            $myMock = $this->createMock(Meter::class);
            $myMock->method('add_meter')->willReturn("Success"||"Error");
            $result = $myMock->add_meter($meter_id, $meter_account, $meter_owner, $meter_address);
            $this->assertEquals($result,TRUE);
        }
            // testing the add meter function
        public function testAddMeter(){
            $meter_id = "1234569845"; 
            $meter_account = 25.00 ;
            $meter_owner = "Cyto Owusu"; 
            $meter_address = "Ashtown, Kumasi";
            $myMock = $this->createMock(Meter::class);
            $myMock->method('add_meter')->willReturn("Success");
            $result = $myMock->add_meter($meter_id, $meter_account, $meter_owner, $meter_address);
            $this->assertTrue($result == "Success");
        }

            // testing the add alias meter function
        public function testAddAliasMeter(){
            $meter_id = "1234569845"; 
            $customer_id = "09873456123" ;
            $meter_alias = "RM21"; 
            
            $myMock = $this->createMock(Meter::class);
            $myMock->method('add_alias_meter')->willReturn("Success");
            $result = $myMock->add_alias_meter($meter_id, $customer_id, $meter_alias);
            $this->assertTrue($result == "Success");
        }

            // testing the update meter function
        public function testUpdateMeter(){
            $meter_id = "1234569845"; 
            $meter_account = 25.00 ;
            $meter_owner = "Cyto Owusu"; 
            $meter_address = "Ashtown, Kumasi";
            $item_no = 1;
            
            $myMock = $this->createMock(Meter::class);
            $myMock->method('update_meter')->willReturn("Success");
            $result = $myMock->update_meter($meter_id, $meter_account, $meter_owner, $meter_address, $item_no);
            $this->assertTrue($result == "Success");
        }

                // testing the update alias meter function
        public function testUpdateAlias(){
            $meter_id = "1234569845"; 
            $customer_id = "09873456123" ;
            $meter_alias = "RM21"; 
            
            $myMock = $this->createMock(Meter::class);
            $myMock->method('update_alias_meter')->willReturn("Success");
            $result = $myMock->update_alias_meter($meter_id, $customer_id, $meter_alias);
            $this->assertTrue($result == "Success");
        }

            // testing the deduct cost  function
        public function testDeductCost(){
            $meter_id = "1234569845"; 
            $amount_due = 11 ;
            
            $myMock = $this->createMock(Meter::class);
            $myMock->method('deduct_cost_of_water')->willReturn("Success");
            $result = $myMock->deduct_cost_of_water($meter_id, $amount_due);
            $this->assertTrue($result == "Success");
        }

            // testing the topup function
        public function testMeterTopup(){
            $meter_id = "1234569845"; 
            $amount = 11 ;
            
            $myMock = $this->createMock(Meter::class);
            $myMock->method('meter_top_up')->willReturn("Success");
            $result = $myMock->meter_top_up($meter_id, $amount);
            $this->assertTrue($result == "Success");
        }

            // testing the user lock function
        public function testUserLock(){
            $meter_id = "1234569845"; 
            $myMock = $this->createMock(Meter::class);
            $myMock->method('user_lock')->willReturn("Success");
            $result = $myMock->user_lock($meter_id);
            $this->assertTrue($result == "Success");

        }

            // testing the admin lock function
        public function testAdminLock(){
            $meter_id = "1234569845"; 
            $myMock = $this->createMock(Meter::class);
            $myMock->method('admin_lock')->willReturn("Success");
            $result = $myMock->admin_lock($meter_id);
            $this->assertTrue($result == "Success");

        }

            // testing the add history function
        public function testAddHistory(){

            $meter_id = "1234569845"; 
            $amount = 21;
            $bal_b4 = 5;
            $bal_aft = 26;
            $action = "Add";
            $myMock = $this->createMock(Meter::class);
            $myMock->method('add_history')->willReturn("Success");
            $result = $myMock->add_history($meter_id, $amount, $bal_b4, $bal_aft,$action);
            $this->assertEquals($result,"Success");

        }

            // testing the find function
        public function testFind(){

            $meter_id = "14357098432"; 
            $table = "meter";
            $data = [
                [
                    "item_no" => 3, "meter_id" => 14357098432, "meter_account" => 0.00, "bal_b4" => -12.00 ,
                    "meter_owner" => "Bale Micks", "meter_address" => "AH-123-456-099", "lock_status" => "UNLOCKED", 
                    "health_status" => "GOOD", "entry_date" => "2021-06-09 19:11:23", "last_updated" => "2021-07-09 16:21:14 ",
                    "admin_lock" => 0, "user_lock" => 1, "borrowed" => "YES", "borrowed_bal" => -13.7500, 
                    "used_amount" => 0.0000
                ]
            ];
            $myMock = $this->createMock(Meter::class);
            $myMock->method('find')->willReturn($data);
            $result = $myMock->find($meter_id,$table);
            $this->assertEquals(count($result),1);

        }

            // testing the get all function
        public function testGetAll(){
            $data = [
                [
                    "item_no" => 3, "meter_id" => 14357098432, "meter_account" => 0.00, "bal_b4" => -12.00 ,
                    "meter_owner" => "Bale Micks", "meter_address" => "AH-123-456-099", "lock_status" => "UNLOCKED", 
                    "health_status" => "GOOD", "entry_date" => "2021-06-09 19:11:23", "last_updated" => "2021-07-09 16:21:14 ",
                    "admin_lock" => 0, "user_lock" => 1, "borrowed" => "YES", "borrowed_bal" => -13.7500, 
                    "used_amount" => 0.0000
                ],
                [
                    "item_no" => 3, "meter_id" => 14357098432, "meter_account" => 0.00, "bal_b4" => -12.00 ,
                    "meter_owner" => "Bale Micks", "meter_address" => "AH-123-456-099", "lock_status" => "UNLOCKED", 
                    "health_status" => "GOOD", "entry_date" => "2021-06-09 19:11:23", "last_updated" => "2021-07-09 16:21:14 ",
                    "admin_lock" => 0, "user_lock" => 1, "borrowed" => "YES", "borrowed_bal" => -13.7500, 
                    "used_amount" => 0.0000
                ],
                [
                    "item_no" => 3, "meter_id" => 14357098432, "meter_account" => 0.00, "bal_b4" => -12.00 ,
                    "meter_owner" => "Bale Micks", "meter_address" => "AH-123-456-099", "lock_status" => "UNLOCKED", 
                    "health_status" => "GOOD", "entry_date" => "2021-06-09 19:11:23", "last_updated" => "2021-07-09 16:21:14 ",
                    "admin_lock" => 0, "user_lock" => 1, "borrowed" => "YES", "borrowed_bal" => -13.7500, 
                    "used_amount" => 0.0000
                ]
            ];

            $table = "meter";
           
            $myMock = $this->createMock(Meter::class);
            $myMock->method('get_all')->willReturn($data);
            $result = $myMock->get_all($table);
            $this->assertTrue(count($result) > 0);

        }

            // testing the Delete  function
        public function testDeleteMeter(){

            $meter_id = "14357098432"; 
            $table = "meter";
            $myMock = $this->createMock(Meter::class);
            $myMock->method('delete')->willReturn("Success");
            $result = $myMock->delete($meter_id,$table);
            $this->assertTrue($result == "Success");
        }

            // testing the delete alias meter function
        public function testDeleteMeterAlias(){
            
            $meter_id = "14357098432"; 
            $customer_id = "908745673210";
            $myMock = $this->createMock(Meter::class);
            $myMock->method('delete_alias')->willReturn("Success");
            $result = $myMock->delete_alias($meter_id,$customer_id);
            $this->assertTrue($result == "Success");
        }


        
    }