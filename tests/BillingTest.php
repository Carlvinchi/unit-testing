<?php

    //importing the user module module
    require_once("./modules/Meter.php");
    require_once("./modules/Billing.php");

    class BillingTest extends \PHPUnit\Framework\TestCase {

        // testing the add bill function
        public function testAddBill(){

            $meter_id = "1234569845"; 
            $cost_amount = 23.00 ; 
            
            $myMock = $this->createMock(Billing::class);
            $myMock->method('add_bill')->willReturn("Success");
            $result = $myMock->add_bill($meter_id, $cost_amount);
            $this->assertTrue($result == "Success");
        }

            // testing the add reading function
        public function testAddReading(){

            $meter_id = "1234569845"; 
            $cost = 23.00 ; 
            $reading = 500;
            $volume_consumed = 5;
            
            $myMock = $this->createMock(Billing::class);
            $myMock->method('add_reading')->willReturn("Success");
            $result = $myMock->add_reading($meter_id, $reading,$volume_consumed, $cost);
            $this->assertTrue($result == "Success");
        }

            // testing the get bills function
        public function testGetBills(){
            $meter_id = "12345678";
            $myMock = $this->createMock(Billing::class);
            $myMock->method('get_bills')->willReturn([]);
            $result = $myMock->get_bills($meter_id);
            $this->assertIsArray($result);
        }

        // testing the get reading function
        public function testGetReading(){
            $meter_id = "12345678";
            $myMock = $this->createMock(Billing::class);
            $myMock->method('get_readings')->willReturn([]);
            $result = $myMock->get_readings($meter_id);
            $this->assertIsArray($result);
        }

            // testing the delete function
        public function testDelete(){
            
            $meter_id = "14357098432"; 
            $table = "billing";
            $myMock = $this->createMock(Meter::class);
            $myMock->method('delete')->willReturn("Success");
            $result = $myMock->delete($meter_id,$table);
            $this->assertTrue($result == "Success");
        }

    }