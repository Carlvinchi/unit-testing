This project is for the unit testing assignment

The testing framework used is PHPUnit, 

to run tests, simply run ./vendor/bin/phpunit tests

Tests were created for the methods in the Database, User, Meter, Billing and Payment classes

All tests can be found in tests directory

Class and Database mocking were used extensively in the testing process

Overall 33 tests with 33 assertions

Thank you
